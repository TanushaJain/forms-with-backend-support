import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  user = {
    username: '',
    firstName: '',
    lastName: '',
    password: '',
  };

  userId: number | undefined;

  userForm!: FormGroup;

  constructor(
    private router: Router,
    private usersService: UserService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {}
  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.userId = parseInt(id);
      this.usersService
        .getUser(this.userId)
        .subscribe((user) => (this.user = user));
    }

    this.userForm = this.fb.group({
      username: ['', [Validators.required, Validators.maxLength(2)]],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      password: [
        '',
        [Validators.required, Validators.maxLength(8), Validators.minLength(6)],
      ],
    });
  }

  submit(): void {
    if (this.userId) {
      this.usersService
        .updateUser(this.userId, this.user)
        .subscribe(() => this.home());
    } else {
      this.usersService.addUser(this.user).subscribe(() => this.home());
    }
  }

  home(): void {
    this.router.navigate(['/']);
  }

  get userName() {
    return this.userForm.get('username');
  }

  get password() {
    return this.userForm.get('password');
  }

  get firstname() {
    return this.userForm.get('firstname');
  }

  get lastname() {
    return this.userForm.get('lastname');
  }
}
