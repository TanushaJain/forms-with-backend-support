package com.demo.forms.services;

import java.util.List;

import com.demo.forms.DAO.UsersDAO;
import com.demo.forms.hibernate.entities.User;


public class UsersService {

	UsersDAO DAO = new UsersDAO();
	
	public List<User> getUsers() {
		List<User> list = DAO.getUsers();
		return list;
	}

	public void addUser(User brand) {
		DAO.addUser(brand);
		
	}

	public void updateUser(User updatedUser) {
		DAO.updateUser(updatedUser);
		
	}

	public void deleteUser(int brandId) {
		DAO.deleteUser(brandId);
		
	}

	public User getUser(int userId) {
		return DAO.getUser(userId);
	}

}
