import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  users = [
    {
      userId: 1,
      firstName: 'Tanusha',
      lastName: 'Jain',
      username: 'TJ',
      password: '23081998',
    },
  ];

  constructor(private router: Router, private usersService: UserService) {}
  ngOnInit(): void {
    this.usersService.getUsersList().subscribe((users) => {
      this.users = users;
    });
  }

  addUser(): void {
    this.router.navigate(['/addUser']);
  }

  deleteUser(userId: number) {
    this.usersService.deleteUser(userId).subscribe(() => location.reload());
  }

  editUser(userId: number) {
    this.router.navigate([`/editUser/${userId}`]);
  }
}
