package com.demo.forms.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;  

import com.demo.forms.hibernate.entities.User;
import com.demo.forms.services.UsersService;

@CrossOrigin(origins="http://localhost:4200")  
@Path("/")
public class Users {
	UsersService service = new UsersService();
	
	@GET
	@Path("/users")
	public List<User> getUsers() {
		List<User> list  = service.getUsers();
		return list;
	}
	
	@GET
	@Path("/users/{userId}")
	public User getUser(@PathParam("userId")  int userId) {
		return service.getUser(userId);
	}

	@POST
	@Path("/users")
	public void postUsers(User user) {
		service.addUser(user);
	}
	
	@PUT
	@Path("/users/{userId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void putUsers(@PathParam("userId")  int userId, @RequestBody User updatedUser) {
		updatedUser.setUserId(userId);
		service.updateUser(updatedUser);
	}
	
	@DELETE
	@Path("/users/{userId}")
	public void deleteUsers(@PathParam("userId") int userId) {
		service.deleteUser(userId);
	}

}
