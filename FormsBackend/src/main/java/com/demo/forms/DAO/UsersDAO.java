package com.demo.forms.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.demo.forms.hibernate.entities.User;

public class UsersDAO {
	
	SessionFactory factory = new Configuration()
			                     .configure("hibernate.cfg.xml")
			                     .addAnnotatedClass(User.class)
			                     .buildSessionFactory();

	public List<User> getUsers() {
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		List<User> list = session.createQuery("from users")
				.getResultList();
		return list;
	}

	public void addUser(User user) {
		Session session = factory.getCurrentSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
	
	}

	public void updateUser(User updatedUser) {
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		int id = updatedUser.getUserId();
		User user = session.get(User.class, id);
		user.setFirstName(updatedUser.getFirstName());
		user.setLastName(updatedUser.getLastName());
		user.setPassword(updatedUser.getPassword());
		user.setUsername(updatedUser.getUsername());
		session.update(user);
		session.getTransaction().commit();
		
	}

	public void deleteUser(int userId) {
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		User user = session.get(User.class, userId);
		session.delete(user);
		session.getTransaction().commit();
		
	}

	public User getUser(int userId) {
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		return session.get(User.class, userId);
	}

}




