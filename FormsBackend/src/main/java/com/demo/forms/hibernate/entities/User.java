package com.demo.forms.hibernate.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="users")
@Table(name="users")
public class User {

    @Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", username="
				+ username + ", password=" + password + "]";
	}

	@Id
    @Column(name="user_id")
    int userId;

    @Column (name="first_name")
    String firstName;

    @Column(name="last_name")
    String lastName;

    @Column(name="username")
    String username;

    @Column(name="password")
    String password;

    public User() {
        this.firstName = "";
        this.lastName = "";
        this.username = "";
        this.password = "";
    }

    public User(int user_id, String firstName, String lastName, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.userId= user_id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserId() {
        return this.userId;
    }
    
    public void setUserId(int userId) {
    	this.userId=userId;
    }
}
